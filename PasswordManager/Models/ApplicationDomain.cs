﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PasswordManager.Models
{
    [Serializable]
    [DataContract]
    public class ApplicationDomain
    {
        [DataMember]
        public int ID { get; set; }

        [Required]
        [DataMember]
        public string Name { get; set; }

        public string UserID { get; set; }

        public virtual ICollection<ApplicationPassword> Passwords { get; set; }

        public virtual ApplicationUser User { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ApplicationPassword
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [DataMember]
        [Required]
        public string Password { get; set; }

        [DataMember]
        public int ApplicationDomainID { get; set; }

        public virtual ApplicationDomain ApplicationDomain { get; set; }
    }

}