namespace PasswordManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ApplicationDomains", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.ApplicationPasswords", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ApplicationPasswords", "Password", c => c.String());
            AlterColumn("dbo.ApplicationDomains", "Name", c => c.String());
        }
    }
}
