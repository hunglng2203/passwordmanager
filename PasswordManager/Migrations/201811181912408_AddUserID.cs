namespace PasswordManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserID : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ApplicationDomains", name: "User_Id", newName: "UserID");
            RenameIndex(table: "dbo.ApplicationDomains", name: "IX_User_Id", newName: "IX_UserID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ApplicationDomains", name: "IX_UserID", newName: "IX_User_Id");
            RenameColumn(table: "dbo.ApplicationDomains", name: "UserID", newName: "User_Id");
        }
    }
}
