namespace PasswordManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApplicationDomainsID : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ApplicationPasswords", "ApplicationDomain_ID", "dbo.ApplicationDomains");
            DropIndex("dbo.ApplicationPasswords", new[] { "ApplicationDomain_ID" });
            RenameColumn(table: "dbo.ApplicationPasswords", name: "ApplicationDomain_ID", newName: "ApplicationDomainID");
            AlterColumn("dbo.ApplicationPasswords", "ApplicationDomainID", c => c.Int(nullable: false));
            CreateIndex("dbo.ApplicationPasswords", "ApplicationDomainID");
            AddForeignKey("dbo.ApplicationPasswords", "ApplicationDomainID", "dbo.ApplicationDomains", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationPasswords", "ApplicationDomainID", "dbo.ApplicationDomains");
            DropIndex("dbo.ApplicationPasswords", new[] { "ApplicationDomainID" });
            AlterColumn("dbo.ApplicationPasswords", "ApplicationDomainID", c => c.Int());
            RenameColumn(table: "dbo.ApplicationPasswords", name: "ApplicationDomainID", newName: "ApplicationDomain_ID");
            CreateIndex("dbo.ApplicationPasswords", "ApplicationDomain_ID");
            AddForeignKey("dbo.ApplicationPasswords", "ApplicationDomain_ID", "dbo.ApplicationDomains", "ID");
        }
    }
}
