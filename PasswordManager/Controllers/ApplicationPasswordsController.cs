﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PasswordManager.Models;

namespace PasswordManager.Controllers
{
    public class ApplicationPasswordsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ApplicationPasswords
        public IQueryable<ApplicationPassword> GetApplicationPasswords()
        {
            return db.ApplicationPasswords;
        }

        // GET: api/ApplicationPasswords/5
        [ResponseType(typeof(ApplicationPassword))]
        public IHttpActionResult GetApplicationPassword(int id)
        {
            ApplicationPassword applicationPassword = db.ApplicationPasswords.Find(id);
            if (applicationPassword == null)
            {
                return NotFound();
            }

            return Ok(applicationPassword);
        }

        // PUT: api/ApplicationPasswords/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutApplicationPassword(int id, ApplicationPassword applicationPassword)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != applicationPassword.ID)
            {
                return BadRequest();
            }

            db.Entry(applicationPassword).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationPasswordExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST: api/ApplicationPasswords
        [ResponseType(typeof(ApplicationPassword))]
        public IHttpActionResult PostApplicationPassword(ApplicationPassword applicationPassword)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ApplicationPasswords.Add(applicationPassword);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = applicationPassword.ID }, applicationPassword);
        }

        // DELETE: api/ApplicationPasswords/5
        [ResponseType(typeof(ApplicationPassword))]
        public IHttpActionResult DeleteApplicationPassword(int id)
        {
            ApplicationPassword applicationPassword = db.ApplicationPasswords.Find(id);
            if (applicationPassword == null)
            {
                return NotFound();
            }

            db.ApplicationPasswords.Remove(applicationPassword);
            db.SaveChanges();

            return Ok(applicationPassword);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationPasswordExists(int id)
        {
            return db.ApplicationPasswords.Count(e => e.ID == id) > 0;
        }
    }
}