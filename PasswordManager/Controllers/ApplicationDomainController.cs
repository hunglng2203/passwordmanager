﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PasswordManager.Models;

namespace PasswordManager.Controllers
{
    [Authorize]
    public class ApplicationDomainController : Controller
    {
        private ApplicationUser CurrentUser()
        {
            return System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(User.Identity.GetUserId());
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ApplicationDomain
        public ActionResult Index()
        {
            var currentUserId = User.Identity.GetUserId();
            return View(db.ApplicationDomains.Where(x => x.User.Id == currentUserId).ToList());
        }

        // GET: ApplicationDomain/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var currentUserId = User.Identity.GetUserId();
            ApplicationDomain applicationDomain = db.ApplicationDomains.Include(x => x.Passwords).FirstOrDefault(x => x.ID == id && x.UserID == currentUserId);
            if (applicationDomain == null)
            {
                return HttpNotFound();
            }
            return View(applicationDomain);
        }

        // GET: ApplicationDomain/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApplicationDomain/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] ApplicationDomain applicationDomain)
        {
            if (ModelState.IsValid)
            {
                //applicationDomain.User = new ApplicationUser();
                //applicationDomain.User.Id = User.Identity.GetUserId();
                applicationDomain.UserID = User.Identity.GetUserId();
                db.ApplicationDomains.Add(applicationDomain);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationDomain);
        }

        // GET: ApplicationDomain/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationDomain applicationDomain = db.ApplicationDomains.Find(id);
            if (applicationDomain == null)
            {
                return HttpNotFound();
            }
            return View(applicationDomain);
        }

        // POST: ApplicationDomain/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] ApplicationDomain applicationDomain)
        {
            if (ModelState.IsValid)
            {
                applicationDomain.UserID = User.Identity.GetUserId();
                db.ApplicationDomains.Add(applicationDomain);
                db.Entry(applicationDomain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationDomain);
        }

        // GET: ApplicationDomain/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationDomain applicationDomain = db.ApplicationDomains.Find(id);
            if (applicationDomain == null)
            {
                return HttpNotFound();
            }
            return View(applicationDomain);
        }

        // POST: ApplicationDomain/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApplicationDomain applicationDomain = db.ApplicationDomains.Find(id);
            db.ApplicationDomains.Remove(applicationDomain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
